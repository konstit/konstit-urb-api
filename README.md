﻿
# Konstit Urb API

Especificações e exemplos de uso da API disponível na plataforma Konstit Urb.

# 1. Modelo de avaliação automática (AVM)
---

O modelo de avaliação automática pode ser encarado como uma avaliação expressa. O resultado obtido através desse modelo não substitui um Laudo de Avaliação e deve ser tratado como uma aproximação do valor de mercado do imóvel avaliando dada a sua região.

Ao solicitar por uma avaliação, o modelo buscará, automaticamente, por imóveis similares ao avaliando e realizará os tratamentos necessários para auferir o valor aproximado do imóvel. Esse processo poderá levar alguns segundos. Logo, a resposta do AVM não ocorre de forma instantânea e, por depender de pesquisa de mercado, não possui garantia de sucesso.

## 1.1. Parâmetros de autenticação
---

Para utilizar a API será necessário informar os parâmetros `token` e `group`, tanto nas requisições do tipo `GET` quanto `POST`. Esses parâmetros são disponibilizados mediante a contratação do serviço.

Caso não tenha recebido ou precise alterar o token de acesso envie um e-mail para [contato@konstit.com.br](mailto:contato@konstit.com.br)  
  
## 1.2. Novo pedido de avaliação
---

*Request:* `POST` `https://urb.konstit.com.br/api/avm/register-order`  
*Body:* [parâmetros de autenticação](#markdown-header-11-parametros-de-autenticacao) além daqueles especificados a seguir.  
  
Atualmente, é suportada a avaliação das seguintes categorias de imóveis: apartamentos, casas e conjuntos comerciais. Os parâmetros adicionais variam com a categoria do imóvel, conforme segue:
  
**Apartamentos**  
  
|Parâmetro       |Descrição                          		|Formato                 |
|----------------|------------------------------------------|-----------------------------|
|`category`  |Indica a tipologia do imóvel. | "apartamentos"           |
|`tag`  |Etiqueta opcional associada ao pedido, identificação interna | texto (até 20 caracteres)            |
|`addressZipcode`  |CEP do imóvel. | 00000-000            |
|`addressNumber`   |Número predial do imóvel. |texto                        |
|`area`            |Área privativa do imóvel. |decimal (0.00)               |
|`buildingBedrooms`   |Número total de dormitórios do imóvel. |inteiro           |
|`buildingParking` |Número total de vagas de garagem (cobertas e descobertas) do imóvel. | inteiro |  
  
  
**Casas**  
  
|Parâmetro     		|Descrição                          		|Formato                 |
|-------------------|------------------------------------------|-----------------------------|
|`category`  |Indica a tipologia do imóvel. | "casas" |
|`tag`  |Etiqueta opcional associada ao pedido, identificação interna. | texto (até 20 caracteres)            |
|`addressZipcode`  |CEP do imóvel. | 00000-000            |
|`addressNumber`   |Número predial do imóvel. |texto                        |
|`area`            	|Área construída do imóvel. |decimal (0.00)               |
|`buildingBedrooms`   	|Número total de dormitórios do imóvel. |inteiro           |
|`condominium` 		|Indica se o imóvel está em um condomínio. |booleano (0/1) |  
  
  
**Conjuntos Comerciais**  
  
|Parâmetro       |Descrição                          		|Formato                 |
|----------------|------------------------------------------|-----------------------------|
|`category`  |Indica a tipologia do imóvel. | "conjuntos comerciais"           |
|`tag`  |Etiqueta opcional associada ao pedido, identificação interna | texto (até 20 caracteres)            |
|`addressZipcode`  |CEP do imóvel. | 00000-000            |
|`addressNumber`   |Número predial do imóvel. |texto                        |
|`area`            |Área privativa do imóvel. |decimal (0.00)               |
|`buildingBathrooms`   |Número total de banheiros do imóvel. |inteiro           |
|`buildingParking` |Número total de vagas de garagem (cobertas e descobertas) do imóvel. | inteiro |  
  
  
> O modelo AVM não faz qualquer distinção entre quartos e suítes, o parâmetro `buildingBedrooms` deve representar o total de dormitórios.  

**Retorno**  
Uma resposta no formato `JSON` será retornada após a requisição, indicando o status e um texto informativo.  
  
Exemplo de retorno indicando erro:  
```json
{
	"text": "O CEP do imóvel encontra-se em uma região não suportada pelo método de avaliação automática.",
	"type": "error"
}
```  
  
Em caso de sucesso, um atributo `orderId` único e associado ao pedido será retornado. Esse identificador deverá ser anotado para o acompanhamento futuro do pedido de avaliação:  
```json
{
	"text": "O pedido foi registrado com sucesso.",
	"type": "success",
	"orderId": 1,
	"shareToken": "MXxToqtb"
}
```  
  
> O parâmetro `shareToken` é um token público de 8 caracteres que possibilita a [visualização do pedido](#markdown-header-14-visualizacao-do-pedido) em formato `HTML`.  
  
**Scripts**  
O arquivo [register-order-avm.js](samples/nodejs/register-order-avm.js) do repositório exemplifica a requisição de um novo pedido de avaliação em Node.js  
   
## 1.3. Acompanhamento do pedido
---

*Request:* `GET` `https://urb.konstit.com.br/api/avm/get-order`  
*Query string:* [parâmetros de autenticação](#markdown-header-11-parametros-de-autenticacao) e `orderId`
  
Exemplo:  
```
https://urb.konstit.com.br/api/avm/get-order?token=...&group=...&orderId=...
```  
   
> O parâmetro `orderId` é obtido através de um [novo pedido de avaliação](#markdown-header-12-novo-pedido-de-avaliacao) realizado com sucesso.  
  
**Retorno**  
Uma resposta em formato `JSON` será retornada para a requisição.  
  
Exemplo de retorno indicando erro:
```json
{
	"text": "O id do pedido é inválido.",
	"type": "error"
}
```  
  
Em caso de sucesso, os dados do pedido processado serão retornados. Dentre eles o status, que pode assumir os seguintes valores:  
   
|Status     		|Descrição                          		
|-------------------|------------------------------------------
|pesquisa insuficiente  |O pedido de avaliação não possui solução devido à falta de amostras que se assemelhem ao imóvel avaliando.
|dados inválidos  |O pedido de avaliação não possui solução devido à inserção de dados inválidos para o imóvel avaliando.
|cálculo esgotado  |O pedido de avaliação não pode ser concluído após diversas tentativas.
|pesquisa pendente  |O pedido de avaliação encontra-se na fila para a pesquisa de mercado automática.
|erro de endereçamento  |O motor de busca não encontrou as informações de endereçamento do imóvel avaliando.
|erro de localização  |O motor de busca não encontrou as informações de geolocalização do imóvel avaliando.
|cálculo pendente  |A pesquisa de mercado foi finalizada e o motor de cálculos avaliatórios procederá com a avaliação.
|concluído  |O pedido de avaliação foi concluído com sucesso.  
  
> Os diferentes status servem o propósito de depuração e comunicação em caso de falhas. Na prática, apenas os pedidos com status "concluído" possuem avaliação válida.  
  
Exemplo de resposta, comentado, para um pedido com status "concluído":  
```json
{
   "id": @orderId,
   "tag": @tag, // Etiqueta, opcional
   "method": "avm",
   "category": "casas",
   "status": "concluído", // Status de sucesso no formato texto português
   "register": "", // Data e hora de registro no formato YYYY-MM-DD HH:mm:ss
   "shareToken": "", // Token público de 8 caracteres
   "appraisalPrices": [
      // Valores arredondados (mín., méd. e máx.) da avaliação
      0,
      0,
      0
   ],
   "appraisalUnitPrices": [ 
      // Valores arredondados (mín., méd. e máx.) do m² da avaliação
      0, 
      0,
      0
   ],
   "location": {
      // Dados de endereçamento do imóvel avaliando
      "address": "", // Endereço completo formatado
      "zone": "", // Bairro
      "city": "", // Cidade
      "state": "", // Sigla do estado
      "address_1": "", // Tipo de logradouro
      "address_2": "", // Logradouro
      "zipcode": @addressZipcode,
      "number": @addressNumber,
      "coords": {
         "lat": -00.000000,
         "lng": 00.000000
      }
   },
   "appraisalAttributes": [
      // Os atributos do imóvel avaliando variam com a categoria, nesse exemplo "casas"
      "Área Construída",
      "Dormitórios",
      "Condomínio"
   ],
   "appraisalValues": [
      // Valores referentes a cada atributo retornado anteriormente
      @area,
      @buildingBedrooms,
      @condominium // Condomínio, valor booleano no formato texto português ("sim"/"não")
   ],
   // Exemplos de amostras da pesquisa de mercado utilizadas na avaliação
   "dataSampling": [
      {
         "lat": 00.000000,
         "lng": -00.000000,
         "closest": 1, // Se verdadeiro significa que é a amostra mais similar ao avaliando
         "attributes": "Área: 0 m², Valor Ofertado: R$ 0.000.000, Dormitórios: 0, Condomínio: sim", // Características da amostra no formato texto português
         "address": "" // Endereço reduzido da amostra (apenas logradouro e bairro)
      },
      {
         "lat": -00.000000,
         "lng": 00.000000,
         "closest": 0,
         "attributes": "",
         "address": ""
      }
      // Outras amostras de mercado poderão ser enviadas...
   ],
   // As notas técnicas da avaliação no formato texto português
   "techNotes": "",
   // Nota atribuída pelo sistema para a avaliação
   "grade": "C"
}
```  
  
> O atributo `techNotes` retornado é de apresentação essencial, pois possui informações fundamentais para a análise dos resultados e o apoio à tomada de decisão.  

Para a resposta dos demais status (não concluídos) serão suprimidos atributos como: `appraisalPrices`, `appraisalUnitPrices`, `dataSampling`, `techNodes` e `grade`.  

No caso de status "pesquisa pendente", "erro de endereçamento" ou "erro de localização", por exemplo, o atributo `location` será suprimido ou constará de forma parcial.  

**Scripts**  
O arquivo [get-order-avm.js](samples/nodejs/get-order-avm.js) do repositório exemplifica o acompanhamento de um pedido de avaliação em Node.js  

## 1.4. Visualização do pedido
---

*Request:* `GET` `https://urb.konstit.com.br/api/avm/view-order`  
*Query string:* `group`, `orderId` e `shareToken`  
  
Exemplo:  
```
https://urb.konstit.com.br/api/avm/view-order?group=...&orderId=...&shareToken=...
```  
  
> Os parâmetros `orderId` e `shareToken` são obtidos através de um [novo pedido de avaliação](#markdown-header-12-novo-pedido-de-avaliacao) realizado com sucesso.  
  
Embora seja recomendada a requisição e armazenagem da avaliação no formato `JSON`, em alguns cenários pode ser suficiente ou mais prática a implementação através de link, modal, frame ou popup com a exibição do conteúdo `HTML` em modelo formatado pela Konstit.  
  
## 1.5. Retorno automático (opcional)
---

Oferecemos o cadastro de uma URL de retorno automático.   

Esse mecanismo funciona de forma simples: após a execução dos cálculos avaliatórios, nosso sistema irá reportar os resultados obtidos para um *endpoint* especificado pelo cliente. A requisição a ser realizada é do tipo `POST`, contendo o parâmetro `order` em formato `JSON`.  
  
> Em caso de falha na comunicação com a URL especificada, o sistema procederá na tentativa de reportar os resultados dos pedidos realizados por até 48 horas.  
  
Exemplo reduzido de script para captura em PHP:  
```php
<?php
define(ORDER_STATUS_FINISHED, "concluído");

$order = $_POST["order"];
$json = json_decode($order, true);

if (!empty($json)) {
    echo("JSON data successfully parsed.");
    
    if (!isset($json['id'])) { // invalid order response
	    // Perhaps check for $json['type'] and $json['text']?
	    return;
    }
    
    // TODO: store JSON string, check order status, etc.
    if ($json['status'] === ORDER_STATUS_FINISHED) {
	    // This order has all valuation attributes
    }
} else {
    echo("Error parsing JSON data.");
}
?>
```  
  
Para ativar esse recurso, envie um e-mail para [contato@konstit.com.br](mailto:contato@konstit.com.br) indicando uma URL de retorno para cadastro.   
