const axios = require('axios');
const qs = require('qs');

const group = '';
const token = '';

const order = {
    // Casa
    group: group,
    token: token,
    tag: null,
    category: 'casas',
    addressZipcode: '00000-000',
    addressNumber: '10',
    area: 300,
    buildingBedrooms: 3,
    condominium: 0
};

/*const order = {
    // Apartamento
    group: group,
    token: token,
    tag: null,
    category: 'apartamentos',
    addressZipcode: '00000-000',
    addressNumber: '10',
    area: 110.5,
    buildingBedrooms: 3,
    buildingParking: 2
}*/

/*const order = {
    // Conjuntos Comerciais
    group: group,
    token: token,
    tag: null,
    category: 'conjuntos comerciais',
    addressZipcode: '00000-000',
    addressNumber: '10',
    area: 200,
    buildingBathrooms: 5,
    buildingParking: 3
}*/

axios.post(
    'https://urb.konstit.com.br/api/avm/register-order',  
    qs.stringify(order)
)
.then(resp => {
    // Dataset
    console.log(resp.data);

    // Public html result page
    console.log(`https://urb.konstit.com.br/api/avm/view-order?group=${group}&orderId=${resp.data['orderId']}&shareToken=${resp.data['shareToken']}`);
})
.catch(e => {
    console.log(e);
});